import pytest
from cenzontle.corpus import Corpus

##Tests for starting a corpus
def test_Corpus_loads_json():
    path = 'corpus.json'
    corpus = Corpus(path)
    assert corpus.data == {'speakers': [{'id': 1,
   'audios': ['/file1', '/file2', '/file3'],
   'properties': {'gender': 'F', 'age': '35', 'nationality': 'colombian'}},
  {'id': 2,
   'audios': ['/file21', '/file22', '/file23'],
   'properties': {'gender': 'M', 'age': '22', 'nationality': 'mexican'}},
  {'id': 3,
   'audios': ['/file31', '/file32', '/file33'],
   'properties': {'gender': ' ', 'age': '65', 'nationality': 'argentinian'}}]}

def test_Corpus_empty_path():
    corpus = Corpus()
    assert  corpus.data == {'speakers': []}

def test_simple_idList_check():
    path = 'corpus.json'
    corpus = Corpus(path)
    assert corpus.idList == [1,2,3]

#Tests for infering json

#Tests for filters
def test_filters_simple_id_check():
    path = 'corpus.json'
    corpus = Corpus(path)
    assert corpus.filters({'id':1}) == [{"id": 1,
           "audios": ["/file1", "/file2","/file3"],
       "properties": {"gender": "F", "age":"35", "nationality":"colombian" }}]
    assert corpus.filters({'id':2}) == [{"id": 2,
           "audios": ["/file21", "/file22","/file23"],
       "properties": {
  		            "gender": "M", "age":"22", "nationality":"mexican"
                        }
                    }]
    assert corpus.filters({'id':3}) == [{"id": 3,
           "audios": ["/file31", "/file32","/file33"],
       "properties": {
  		            "gender": " ", "age":"65", "nationality":"argentinian"
                        }
                    }]

def test_filters_numerical():
    path = 'corpus.json'
    corpus = Corpus(path)

    assert corpus.filters({'age': "35"}) == [{"id": 1,
           "audios": ["/file1", "/file2","/file3"],
       "properties": {
  		            "gender": "F", "age":"35", "nationality":"colombian"} }]
    assert corpus.filters({'age': {'gt':40}}) == [ {"id": 3,
           "audios": ["/file31", "/file32","/file33"],
       "properties": {
  		            "gender": " ", "age":"65", "nationality":"argentinian" }}] 

    assert corpus.filters({'age': {'gt':30, 'lt':60}}) == [{"id": 1,
           "audios": ["/file1", "/file2","/file3"],
       "properties": {
  		            "gender": "F", "age":"35", "nationality":"colombian"}}]

    assert corpus.filters({'age': {'lt':20}}) == []

    assert corpus.filters({'age': {'lt':30}}) == [{"id": 2,
           "audios": ["/file21", "/file22","/file23"],
       "properties": {
  		            "gender": "M", "age":"22", "nationality":"mexican"}}]

#############Make test for random choice

def test_properties_filters():
    path = 'corpus.json'
    corpus = Corpus(path)

    assert corpus.filters() == []
    assert corpus.filters({}) == []
    assert corpus.filters({'profession':'teacher'}) == []
    assert corpus.filters({"gender": "M"}) == [{"id": 2,
           "audios": ["/file21", "/file22","/file23"],
       "properties": {
  		            "gender": "M", "age":"22", "nationality":"mexican"}}]
    assert corpus.filters({"gender": "F", 'age':{'gt':60}}) == []
    assert corpus.filters({"gender": "M", 'age':{'gt':18, 'lt':70}}) == [{"id": 2,
           "audios": ["/file21", "/file22","/file23"],
       "properties": {
  		            "gender": "M", "age":"22", "nationality":"mexican"}}]


#Tests for adding speakers
def test_simple_add_speaker():
    path = 'corpus.json'
    corpus = Corpus(path)
    corpus.add_speaker(['file4','file9'], {'pets':True, 'singer':False}, 925)

    assert corpus.data == {'speakers': [{'id': 1,
   'audios': ['/file1', '/file2', '/file3'],
   'properties': {'gender': 'F', 'age': '35', 'nationality': 'colombian'}},
  {'id': 2,
   'audios': ['/file21', '/file22', '/file23'],
   'properties': {'gender': 'M', 'age': '22', 'nationality': 'mexican'}},
  {'id': 3,
   'audios': ['/file31', '/file32', '/file33'],
   'properties': {'gender': ' ', 'age': '65', 'nationality': 'argentinian'}},
   {'id':925, 'audios':['file4','file9'],'properties':{'pets':True, 'singer':False}}]}


def test_add_speaker_no_id():
    path = 'corpus.json'
    corpus = Corpus(path)
    corpus.add_speaker(['file4','file9'], {'pets':True, 'singer':False})

    assert corpus.data == {'speakers': [{'id': 1,
   'audios': ['/file1', '/file2', '/file3'],
   'properties': {'gender': 'F', 'age': '35', 'nationality': 'colombian'}},
  {'id': 2,
   'audios': ['/file21', '/file22', '/file23'],
   'properties': {'gender': 'M', 'age': '22', 'nationality': 'mexican'}},
  {'id': 3,
   'audios': ['/file31', '/file32', '/file33'],
   'properties': {'gender': ' ', 'age': '65', 'nationality': 'argentinian'}},
   {'id':4, 'audios':['file4','file9'],'properties':{'pets':True, 'singer':False}}]}

def test_add_speaker_empty_corpus():
    corpus = Corpus()
    corpus.add_speaker(['file4','file9'], {'pets':True, 'singer':False})
    assert corpus.data == {'speakers': [
            {'id':0, 'audios':['file4','file9'],'properties':{'pets':True, 'singer':False}}]}

    corpusb = Corpus()
    corpusb.add_speaker(['file4','file9'], {'pets':True, 'singer':False}, 'firstSpeaker')
    assert corpusb.data == {'speakers': [
            {'id':'firstSpeaker', 'audios':['file4','file9'],'properties':{'pets':True, 'singer':False}}]}

#Tests for getting speakers
def test_get_speaker():
    path = 'corpus.json'
    corpus = Corpus(path)
    assert corpus.get_speaker({'id':3}) == {"id": 3,
           "audios": ["/file31", "/file32","/file33"],
       "properties": {
  		            "gender": " ", "age":"65", "nationality":"argentinian" }}
    assert corpus.get_speaker({'age':{'gt':60}}) == {"id": 3,
           "audios": ["/file31", "/file32","/file33"],
       "properties": {
  		            "gender": " ", "age":"65", "nationality":"argentinian"}}

    assert corpus.get_speaker({'age':{'gt':70}}) == None

def test_get_speakers():
    path = 'corpus.json'
    corpus = Corpus(path)
    assert corpus.get_speakers({'age':{'lt':70}}) ==  [
      {"id": 1,
           "audios": ["/file1", "/file2","/file3"],
       "properties": {
  		            "gender": "F", "age":"35", "nationality":"colombian"
                        }
                    }, 

        {"id": 2,
           "audios": ["/file21", "/file22","/file23"],
       "properties": {
  		            "gender": "M", "age":"22", "nationality":"mexican"
                        }
                    }, 
        {"id": 3,
           "audios": ["/file31", "/file32","/file33"],
       "properties": {
  		            "gender": " ", "age":"65", "nationality":"argentinian"}}]

    

#Tests for setting a property
def test_set_property_one_speaker():
    path = 'corpus.json'
    corpus = Corpus(path)
    corpus.set_property_speaker(1,{'city':'Medellin'})
    assert corpus.get_speaker({'id':1}) == {"id": 1,
           "audios": ["/file1", "/file2","/file3"],
       "properties": {
  		            "gender": "F", "age":"35", "nationality":"colombian", 'city':'Medellin'} }

def test_set_property_speakers():
    path = 'corpus.json'
    corpus = Corpus(path)
    corpus.set_properties_speakers([1,2,3],{'native':True, 'actor':False})
    assert corpus.speakers == [
      {"id": 1,
           "audios": ["/file1", "/file2","/file3"],
       "properties": {
  		            "gender": "F", "age":"35", "nationality":"colombian",'native':True, 'actor':False
                        }
                    }, 

        {"id": 2,
           "audios": ["/file21", "/file22","/file23"],
       "properties": {
  		            "gender": "M", "age":"22", "nationality":"mexican",'native':True, 'actor':False
                        }
                    }, 
        {"id": 3,
           "audios": ["/file31", "/file32","/file33"],
       "properties": {
  		            "gender": " ", "age":"65", "nationality":"argentinian", 'native':True, 'actor':False
                        }
                    }
      ]
#Tests for getting the properties
def test_get_all_properties():
    path = 'corpus.json'
    corpus = Corpus(path)
    props = corpus.get_properties()
    assert props == {"gender", "age", "nationality"}

def test_get_properties_ids():
    path = 'corpus.json'
    corpus = Corpus(path)
    props = corpus.get_properties([1,2])
    assert props == {"gender", "age", "nationality"}


#Tests for getting the paths    
def test_get_file_paths_by_id():
    path = 'corpus.json'
    corpus = Corpus(path)
    assert corpus.get_file_paths({"id":1}) == ["/file1", "/file2","/file3"]
    assert corpus.get_file_paths({"id":2}) == ["/file21", "/file22","/file23"]
    assert corpus.get_file_paths({"id":3}) == ["/file31", "/file32","/file33"]

def test_simple_get_file_paths():
    path = 'corpus.json'
    corpus = Corpus(path)
    assert corpus.get_file_paths({'gender':'F','age':42}) == []
    assert corpus.get_file_paths({'gender':'F','age':'35'}) == ["/file1", "/file2","/file3"]

def test_get_file_num_filters():
    path = 'corpus.json'
    corpus = Corpus(path)
    assert corpus.get_file_paths({'age':{'gt':'12'}}) == ["/file1", "/file2","/file3",
                                                                "/file21", "/file22","/file23",
                                                                "/file31", "/file32","/file33"]
    assert corpus.get_file_paths({'age':{'lt':'80'}}) == ["/file1", "/file2","/file3",
                                                                "/file21", "/file22","/file23",
                                                                "/file31", "/file32","/file33"]
    assert corpus.get_file_paths({'age':{'lt':'12'}}) == []
    assert corpus.get_file_paths({'age':{'gt':'80'}}) == []
    assert corpus.get_file_paths({'age':{'gt':'8', 'lt':60}}) == ["/file1", "/file2","/file3",
                                                                "/file21", "/file22","/file23"]
    assert corpus.get_file_paths({'age':{'gt':'8', 'lt':60}, 'gender':'F'}) == ["/file1", "/file2","/file3"]

def test_get_one_path():
    path = 'corpus.json'
    corpus = Corpus(path)
    assert corpus.get_file_path({'age':{'gt':'12'}}) == "/file1"

#Tests for statistics    
def test_statistics():
    path = 'corpus_voxforge.json'
    corpus = Corpus(path)
    corpus.statistics_from_json()
    assert len(corpus.speakers) == 12 
    assert len(corpus.recordings) == 24 
    assert corpus.acc_sr[0] == 16000.0
    assert round((sum(corpus.acc_duration)/len(corpus.acc_duration)), 2) == 8.71
    assert round(min(corpus.acc_duration),2) == 5.62
    assert round(max(corpus.acc_duration),2) == 14.00
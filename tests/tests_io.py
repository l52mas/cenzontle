import pytest
from cenzontle.io import *

def test_spectrogram():
    io = Io()
    audio_path = "./data/mini_voxforge/latin_america/female/F_0033/VXSP_F_0033_LAT_0000.wav"
    io.spectrogram(audio_path)
    assert io.tensor.size() == torch.Size([1, 201, 701])
import torch
import torchaudio
import matplotlib.pyplot as plt
import pylab

"""
    A class used to create a spetrogram and modificate it 
    ...
    
    
    Methods
    -------
    spectrogram():
    
         Inputs
         ------
             audio_path: str
                 audio path from which the spectrogram is generated 
           
         Return
         ------
             
    
    
    
    
    mel():
        
    mfcc():
"""




class Io:
    
        def __init__(self):
            pass
       
        
        def spectrogram(self,audio_path = None):
            waveform, sample_rate = torchaudio.load(audio_path)
            print("Shape of waveform: {}".format(waveform.size()))
            print("Sample rate of waveform: {}".format(sample_rate))

            # Plot de waveforms

            # plt.figure()
            # plt.plot(waveform.t().numpy())

            tensor = torchaudio.transforms.Spectrogram()(waveform) #many arguments to modificate the spectrogram 

            print("Shape of spectrogram: {}".format(tensor.size()))

            plt.figure()
            pylab.matshow(tensor.log2()[0,:,:].numpy(), cmap=pylab.get_cmap('plasma'))

            self.tensor = tensor


        def mel(self, audio_path = None, n_mels = 128,f_min = 0.0, f_max = None, n_stft = None, norm = None):
            """Implementation of torchaudio.transforms.MelScale. 
                Turn a normal STFT into a mel frequency STFT, using a conversion matrix. 
                This uses triangular filter banks.
                
                Inputs
                ------
                    audio_path: str
                        Path to the file that contains the audio.
                    n_mels: int, default 128
                        Number of mel filterbanks.
                    sample_rate: int, default 1600

                    f_min: float, default 0
                        Minimum frequency. 
                    f_max: float, default sample_rate // 2 (sample_rate obtained by torchaudio.load)
                        Maximum frequency.
                    n_stft: int, default None
                        Number of bins in STFT. Calculated from first input if None is given.
                    norm: str, default None

                Forward
                -------
                    Parameters:
                    
                        specgram: Tensor
                            A spectrogram STFT of dimension (…, freq, time).

                    Return: Tensor
                            Mel frequency spectrogram of size (…, n_mels, time)."""
            waveform, sr = torchaudio.load(audio_path)
            melScale = torchaudio.transforms.MelScale(n_mels = n_mels, sample_rate = sr, f_min = f_min, f_max = f_max, n_stft = n_stft, norm = norm)(waveform)
            self.melScale = melScale
            return melScale

        def mfcc(self):
            pass


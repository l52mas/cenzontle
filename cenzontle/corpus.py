import json
import os
import essentia
import essentia.standard

class Corpus:
    """
    A class used to upload the corpus to train the model

    
    Inputs
    ------
    json_path : str, default None
        Path to the json file  that describes the corpus.
        The json file must have the following structure:

        {
            'speakers' :[{'id': id, 
                         'audios': [paths_to_audios ],
	                     'properties': {properties of the corpus} ]
        }
        If it is set to None, the corpus will be empty.


    Attributes
    ----------
    json_path: str
        Path to the json file  with the structure of  the corpus.

    data: dic
        Description of the copus
        {
            'speakers' :[{'id': id, 
                         'audios': [paths_to_audios ],
	                     'properties': {properties of the corpus} ]
        }

    idList: list
        List with ids of the speakers in the corpus.
    
    speakers: list
        Speakers in the corpus.

        
    
    Methods
    -------
    infer_json()

    add_speaker()
        Adds one speaker to the corpus

        Inputs
        ------
        audios: list
            The elements of the list must be strings with the path to the audios of the new speaker.

        properties: dict
            Contains the properties of the new speaker.

        id: any, recomended int, default None
            id_speaker of the new speaker. If it is set to None and the id_speaker of previous speakers
            are int, then it is set to the new integer.
        
    add_speakers(ls):
        '''Add speakers to self.speakers.
        
        Input
        -----
            ls: list
                List of dictionaries. Every dictionary represents one speaker to be added.
                The shape of the dictionary must be as follows
                {"audios":[paths to audio files], "properties":{properties of the speaker}, "id":(optional)}'''

    
    set_property_speaker(self, uid, properties):
        Add or modify the properties of one speaker
        
           Inputs
           ------ 
                uid: 
                    id of the spekar to be modified.
                    
                properties: dic
                    Dictionary with the new properties.

    set_properties_speakers( ids, properties):
        Add or modify the properties of a list of speakers
        
           Inputs
           ------ 
                ids: ls, set.
                    Contains the ids of the speakers to be modified.
                    
                properties: dic
                    Dictionary with the new properties.

    get_properties(uids = None):
        Get the available properties of a group of speakers.
           
           Input
           -----
                uids: list, set, default = None
                    Contains the ids of the speakers whose properties we are looking for. If it is None,
                    it returns the properties of all the speakers in the corpus.

           Return
           ------
                set:
                    set with the available properties  in at least on of the speakers past. 
           

    get_speaker(properties):
        Returns one speaker that meets all of the properties.
        
            Input
            -----
                properties: dic
                    Dictionary with the filters that the speaker must satisfy.
                    
            Return
            ------
                Dictionary that represents the speaker.


    get_speakers(properties):
        Returns all the speakers that meet the properties.
        
           Input
           -----
                properties: dic
                    Dictionary with the filters that the speakers must satisfy.
                    
           Return
           ------
                List of dictionaries that represent the speaker.

    
    statistics_from_json(filter,filtered = None, save_report=False)
       
        Generate a general statistics from a json file

        Inputs
        ------    
        
        filtered: list
            list of speakers from which statistics will be obtained in case it is desired to obtain statistics on a list of filtered speakers this variable should be said filtered list
        
        save_report: boolean
            Variable that define if a file (.txt) is created to save the statistics
       
        Return
        ------
            'dataset_statistics.txt': In ./data if 'save_report' is True, it's a text file with the statistics.
    
    
    get_file_path(properties=False,index=0):
        Get only one file path for the specified filters and id
        
        Inputs
        ------
        filters: dict, default False
            filters of the speakers. To express simple conditions use filtes = {'property': 'desired_value'}
            To express numerical filters, use filters = {property:{'gt':num}} for > num,
                                    and filters = {property:{'lt':num}} for < num
        
        index: int, default 0
            index to select one of the file paths that meet the conditions.

        
        Return
        -------
        file_path: str,
            path of one file that meets the filters or id
            
        
    get_file_paths(properties=False):
        Get the  paths of the files for the specified filters and id
        
        Inputs
        ------
        properties: dict, default False
            filters of the speakers. To express simple conditions use 
                                                filtes = {'property': 'desired_value'}
            To express numerical filters, use 
                                        filters = {property:{'gt':num}} for > num,
                                    and filters = {property:{'lt':num}} for < num

        Return
        -------
        file_path: lst,
            List with the paths of the files that meet the filters and id
            If id == False and filters == False and function == False, it returns  random paths
        
    to_json(path = None):
        Untested!! It transforms the corpus to json format and save it.
        
            Input:
                path: string, default None
                    Path-name of the file where the json will be saved. If it is not, it will not be saved.
            
            Return
            ------
                String:
                    Corpus in json format.
    """

    def __init__(self,json_path=None):
        import json
        self.json_path = json_path
        if json_path:
            with open(json_path) as json_file:
                data = json.load(json_file)  
            self.data = data
        else:
            self.data = {'speakers':[]}
            
        self.idList = [a['id'] for a in self.data['speakers']]
        self.speakers = self.data['speakers']
        #If there are repeated ids, ask to mix them or rename.

          
    def infer_json():
        pass

    def filters(self, properties=None):
        '''Choose the speakers based on the given properties.
        
        Inputs
        ------
        properties: dict, default False
            filters of the speakers. To express simple conditions use 
                                                filtes = {'property': 'desired_value'}
            To express numerical filters, use 
                                        filters = {property:{'gt':num}} for > num,
                                    and filters = {property:{'lt':num}} for < num

        Return
        ------
        filtered: list
            List with the speakers that meets all of the filters given in properties.
        '''
        speakers = self.data['speakers']
        if not properties: return []
        def booleanFilter(speaker):
            check = True
            localProperties = properties.copy()
            if 'id' in localProperties:
                if speaker['id'] != localProperties['id']:
                    check = False
                del localProperties['id']

            for property, value in localProperties.items():
                if property not in speaker['properties']:
                    check = False

                elif type(value) == dict:
                    if 'gt' in value:
                        if float(speaker['properties'][property]) <= float(localProperties[property]['gt']):
                            check = False
                    if 'lt' in value:
                        if float(speaker['properties'][property]) >= float(localProperties[property]['lt']):
                            check = False
                    
                elif speaker['properties'][property] != localProperties[property]:
                    check = False
                    
            return check
        
        filtered = list(filter(booleanFilter,speakers))
        
        self.filtered = filtered
        
        return filtered 


    def add_speaker(self, audios, properties, uid = None):
        '''Add one speaker to self.speakers.

        Inputs
        ------
            audios: list
                List with the paths that correspond to the audio files of the speaker to be added.

            properties:dic
                Dictionary with the properties of the new speaker. 
            uid: default None
                If there is no uid specified, the function will try to assign one.

        '''
        
        assert uid not in self.idList
        assert type(audios) == list
        assert type(properties) == dict

        if not uid:
            if  self.idList:
                uid = max(self.idList)+1
            else:
                uid = 0
        speaker = {'id': uid, 'audios':audios, 'properties':properties}
        self.data['speakers'].append(speaker)

    def add_speakers(self, ls):
        '''Add speakers to self.speakers.
        
        Input
        -----
            ls: list
                List of dictionaries. Every dictionary represents one speaker to be added.
                The shape of the dictionary must be as follows
                {"audios":[paths to audio files], "properties":{properties of the speaker}, "id":(optional)}'''

        for speaker in ls:
            if 'id' not in speaker: 
                uid = None
            else:
                uid = speaker['id']
            self.add_speaker(speaker['audios'], speaker['properties'],uid)

    def get_speaker(self, properties):
        '''Returns one speaker that meets all of the properties.
        
            Input
            -----
                properties: dic
                    Dictionary with the filters that the speaker must satisfy.
                    
            Return
            ------
                Dictionary that represents the speaker.'''
        sp = self.filters(properties)
        if sp: return self.filters(properties)[0]
        return None

    def get_speakers(self, properties):
        '''Returns all the speakers that meet the properties.
        
           Input
           -----
                properties: dic
                    Dictionary with the filters that the speakers must satisfy.
                    
           Return
           ------
                List of ictionaries that represent the speaker.'''
        return self.filters(properties)

    def set_property_speaker(self, uid, properties):
        '''Add or modify the properties of one speaker
        
           Inputs
           ------ 
                uid: 
                    id of the spekar to be modified.
                    
                properties: dic
                    Dictionary with the new properties.'''
        sp = [s for s in self.data['speakers'] if s['id']==uid]
        if not sp:
            raise Exception('The id {} was not found in the corpus'.format(uid))
        speaker = sp[0]
        for key,value in properties.items():
            speaker['properties'][key] = value
        self.idList = [a['id'] for a in self.data['speakers']] #the ids could have been changed
        self.speakers = self.data['speakers']
        
        
    
    def set_properties_speakers(self, ids, properties):
        '''Add or modify the properties of a list of speakers
        
           Inputs
           ------ 
                ids: ls, set.
                    Contains the ids of the speakers to be modified.
                    
                properties: dic
                    Dictionary with the new properties.'''

        for e in ids:
            self.set_property_speaker(e, properties)

    def get_properties(self, uids = None):
        '''Get the available properties of a group of speakers.
           
           Input
           -----
                uids: list, set, default = None
                    Contains the ids of the speakers whose properties we are looking for. If it is None,
                    it returns the properties of all the speakers in the corpus.

            Return
            ------
                set:
                    set with the available properties  in at least on of the speakers past. 
           '''
        if not uids: 
            propsList = [speaker['properties'].keys() for speaker in self.speakers]
            propsList = [e for ls in propsList for e in ls]
            return set(propsList)
        propsList = [speaker['properties'].keys() for speaker in self.speakers if speaker['id'] in uids]
        propsList = [e for ls in propsList for e in ls]
        return set(propsList)

    def to_json(self, path = None):
        '''Untested!! It transforms the corpus to json format and save it.
        
            Input:
                path: string, default None
                    Path-name of the file where the json will be saved. If it is not, it will not be saved.
            
            Return
            ------
                String:
                    Corpus in json format.'''
        import json
        jsonData = json.dumps(self.data )
        if path:
            f = open(path, "x")
            f.close()
            f = open(path, "w")
            f.write(jsonData)
            f.close()
        return jsonData

    def get_file_paths(self, properties=False):
        '''Get the  paths of the files for the specified filters and id
        
        Inputs
        ------
        properties: dict, default False
            filters of the speakers. To express simple conditions use 
                                                filtes = {'property': 'desired_value'}
            To express numerical filters, use 
                                        filters = {property:{'gt':num}} for > num,
                                    and filters = {property:{'lt':num}} for < num

        Return
        -------
        file_path: lst,
            List with the paths of the files that meet the filters and id
            If id == False and filters == False and function == False, it returns  random paths
        '''
        file_paths = self.filters(properties)
        file_paths = [speaker['audios'] for speaker in file_paths]
        file_paths = [path for paths in file_paths for path in paths]
        return file_paths

    
    def get_file_path(self, properties=False,index=0):
        '''Get only one file path for the specified filters and id
        
        Inputs
        ------
        filters: dict, default False
            filters of the speakers. To express simple conditions use filtes = {'property': 'desired_value'}
            To express numerical filters, use filters = {property:{'gt':num}} for > num,
                                    and filters = {property:{'lt':num}} for < num
        
        index: int, default 0
            index to select one of the file paths that meet the conditions.

        
        Return
        -------
        file_path: str,
            path of one file that meets the filters or id
            
        '''        
        return self.get_file_paths(properties)[index]

    def statistics_from_json(self,filtered = None, save_report = False):
        if filtered:
            speakers = filtered
        else:
            speakers = self.data['speakers']
        audios=[]
        for speaker in speakers:
            for key in speaker.keys():
                if key == 'audios':
                    for audio in speaker['audios']:
                        audios.append(audio)

        acc_sr=[]
        acc_duration=[]

        for audio in audios:
            try:
                loader = essentia.standard.AudioLoader(filename=audio)
                audio,sr,nc, md5, bt, codec=loader()
                acc_sr.append(sr)
                acc_duration.append(len(audio)/sr)
            except RuntimeError:
                del audio  

        
        self.speakers = speakers
        self.recordings = audios
        self.acc_sr = acc_sr
        self.acc_duration = acc_duration

        print("{0: <30} :{1: >18}".format('Total of speakers', len(speakers)))
        print("{0: <30} :{1: >18}".format('Total of recordings', len(audios)))
        print("{0: <30} :{1: >23}".format("Average sample rate",sum(acc_sr)/len(acc_sr)))
        print("{0: <30} :{1: >20.2f} segs".format("Average duration",sum(acc_duration)/len(acc_duration)))
        print("{0: <30} :{1: >20.2f} segs".format("Minimum duration",min(acc_duration)))
        print("{0: <30} :{1: >21.2f} segs".format("Maximum duration",max(acc_duration)))

        if save_report:
            file = open("./data/dataset_statistics.txt", "w")
            file.write("{0: <30} :{1: >18}".format('Total of speakers', len(speakers)) + os.linesep)
            file.write("{0: <30} :{1: >18}".format('Total of recordings', len(audios)) + os.linesep)
            file.write("{0: <30} :{1: >23}".format("Average sample rate",sum(acc_sr)/len(acc_sr))+ os.linesep)
            file.write("{0: <30} :{1: >20.2f} segs".format("Average duration",sum(acc_duration)/len(acc_duration)) + os.linesep)
            file.write("{0: <30} :{1: >20.2f} segs".format("Minimum duration",min(acc_duration))+ os.linesep)
            file.write("{0: <30} :{1: >21.2f} segs".format("Maximum duration",max(acc_duration))+ os.linesep)
            file.close()
